<?php

function kvantstudio_exchange_rates_autoload($classname) {
  $classname = preg_replace('/^[A-Z]+\\\/', '', $classname);
  $filename = __DIR__ . DIRECTORY_SEPARATOR . $classname . '.php';
  if (is_readable($filename)) {
    require $filename;
  }
}

spl_autoload_register('kvantstudio_exchange_rates_autoload', true, true);
