# Сборка основана на исходниках nabarabane/cbr, которая распространялась на момент клонирования (18.04.2019 г.) под лицензией MIT.

# Курсы валют ЦБ России
**Библиотека для получения и парсинга XML-данных о курсах валют**

## Доступные ресурсы
**Котировки на заданный день**
[http://www.cbr.ru/scripts/XML_daily.asp](http://www.cbr.ru/scripts/XML_daily.asp)
Возможно получение как полного списка, так и фильтрация по выбранным кодам валют (USD, EUR и т.д.).

**Динамика котировок выбранной валюты**
[http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=10/07/2015&date_req2=20/07/2015&VAL_NM_RQ=R01235](http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=10/07/2015&date_req2=20/07/2015&VAL_NM_RQ=R01235)

**Вывод данных:** XML или PHP-массив.

## Использование
### Котировки на заданный день
[http://www.cbr.ru/scripts/XML_daily.asp](http://www.cbr.ru/scripts/XML_daily.asp)
```php
<?php

use ExchangeRates\CurrencyDaily;

try {
    $handler = new CurrencyDaily();
    $result = $handler->setDate('20/07/2015')->setCodes(['USD', 'EUR'])->request()->getResult();
} catch (\Exception $e) {
	echo $e->getMessage();
}
```

### Динамика котировок за период времени
Справочник кодов валют формата Банка России - [http://www.cbr.ru/scripts/XML_val.asp](http://www.cbr.ru/scripts/XML_val.asp)
```php
<?php

use ExchangeRates\CurrencyPeriod;

try {
    $result = (new CurrencyPeriod())->setDateFrom('20/06/2015')->setDateTo('20/07/2015')->setCurrency('R01535')->request()->getResult();
} catch (\Exception $e) {
	echo $e->getMessage();
}
```
